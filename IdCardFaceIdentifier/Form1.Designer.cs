﻿namespace IdCardFaceIdentifier
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.ValidDate = new CCWin.SkinControl.SkinTextBox();
            this.skinLabel1 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel2 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel3 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel4 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel5 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel6 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel8 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel7 = new CCWin.SkinControl.SkinLabel();
            this.PeopleName = new CCWin.SkinControl.SkinTextBox();
            this.cardInfoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.PeopleNation = new CCWin.SkinControl.SkinTextBox();
            this.skinLabel9 = new CCWin.SkinControl.SkinLabel();
            this.PeopleSex = new CCWin.SkinControl.SkinTextBox();
            this.PeopleBirthday = new CCWin.SkinControl.SkinTextBox();
            this.PeopleIDCode = new CCWin.SkinControl.SkinTextBox();
            this.Department = new CCWin.SkinControl.SkinTextBox();
            this.skinPictureBox1 = new CCWin.SkinControl.SkinPictureBox();
            this.PeopleAddress = new CCWin.SkinControl.SkinTextBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.skinLabel10 = new CCWin.SkinControl.SkinLabel();
            this.skinPictureBox2 = new CCWin.SkinControl.SkinPictureBox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.skinPictureBox3 = new CCWin.SkinControl.SkinPictureBox();
            this.skinPictureBox4 = new CCWin.SkinControl.SkinPictureBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.skinLabel11 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel12 = new CCWin.SkinControl.SkinLabel();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cardInfoBindingSource)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.skinPictureBox1)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.skinPictureBox2)).BeginInit();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.skinPictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.skinPictureBox4)).BeginInit();
            this.tableLayoutPanel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tableLayoutPanel2.Controls.Add(this.ValidDate, 1, 7);
            this.tableLayoutPanel2.Controls.Add(this.skinLabel1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.skinLabel2, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.skinLabel3, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.skinLabel4, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.skinLabel5, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.skinLabel6, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.skinLabel8, 0, 6);
            this.tableLayoutPanel2.Controls.Add(this.skinLabel7, 0, 7);
            this.tableLayoutPanel2.Controls.Add(this.PeopleName, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.PeopleBirthday, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.PeopleIDCode, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.Department, 1, 6);
            this.tableLayoutPanel2.Controls.Add(this.skinPictureBox1, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.PeopleAddress, 1, 5);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(10, 10);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 8;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(680, 458);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // ValidDate
            // 
            this.ValidDate.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ValidDate.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.SetColumnSpan(this.ValidDate, 2);
            this.ValidDate.DownBack = null;
            this.ValidDate.Icon = null;
            this.ValidDate.IconIsButton = false;
            this.ValidDate.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.ValidDate.IsPasswordChat = '\0';
            this.ValidDate.IsSystemPasswordChar = false;
            this.ValidDate.Lines = new string[0];
            this.ValidDate.Location = new System.Drawing.Point(165, 413);
            this.ValidDate.Margin = new System.Windows.Forms.Padding(5);
            this.ValidDate.MaxLength = 32767;
            this.ValidDate.MinimumSize = new System.Drawing.Size(28, 28);
            this.ValidDate.MouseBack = null;
            this.ValidDate.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.ValidDate.Multiline = true;
            this.ValidDate.Name = "ValidDate";
            this.ValidDate.NormlBack = null;
            this.ValidDate.Padding = new System.Windows.Forms.Padding(5);
            this.ValidDate.ReadOnly = false;
            this.ValidDate.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.ValidDate.Size = new System.Drawing.Size(510, 40);
            // 
            // 
            // 
            this.ValidDate.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ValidDate.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ValidDate.SkinTxt.Font = new System.Drawing.Font("黑体", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ValidDate.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.ValidDate.SkinTxt.Multiline = true;
            this.ValidDate.SkinTxt.Name = "BaseText";
            this.ValidDate.SkinTxt.Size = new System.Drawing.Size(500, 30);
            this.ValidDate.SkinTxt.TabIndex = 0;
            this.ValidDate.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.ValidDate.SkinTxt.WaterText = "";
            this.ValidDate.TabIndex = 16;
            this.ValidDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.ValidDate.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.ValidDate.WaterText = "";
            this.ValidDate.WordWrap = true;
            // 
            // skinLabel1
            // 
            this.skinLabel1.BackColor = System.Drawing.Color.LightGray;
            this.skinLabel1.BorderColor = System.Drawing.Color.White;
            this.tableLayoutPanel2.SetColumnSpan(this.skinLabel1, 3);
            this.skinLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinLabel1.Font = new System.Drawing.Font("微软雅黑", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel1.Location = new System.Drawing.Point(0, 0);
            this.skinLabel1.Margin = new System.Windows.Forms.Padding(0);
            this.skinLabel1.Name = "skinLabel1";
            this.skinLabel1.Size = new System.Drawing.Size(680, 50);
            this.skinLabel1.TabIndex = 0;
            this.skinLabel1.Text = "二代身份证信息";
            this.skinLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // skinLabel2
            // 
            this.skinLabel2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.skinLabel2.AutoSize = true;
            this.skinLabel2.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel2.BorderColor = System.Drawing.Color.White;
            this.skinLabel2.Font = new System.Drawing.Font("微软雅黑", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel2.Location = new System.Drawing.Point(3, 54);
            this.skinLabel2.Name = "skinLabel2";
            this.skinLabel2.Size = new System.Drawing.Size(146, 41);
            this.skinLabel2.TabIndex = 1;
            this.skinLabel2.Text = "姓　　名";
            // 
            // skinLabel3
            // 
            this.skinLabel3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.skinLabel3.AutoSize = true;
            this.skinLabel3.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel3.BorderColor = System.Drawing.Color.White;
            this.skinLabel3.Font = new System.Drawing.Font("微软雅黑", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel3.Location = new System.Drawing.Point(3, 104);
            this.skinLabel3.Name = "skinLabel3";
            this.skinLabel3.Size = new System.Drawing.Size(146, 41);
            this.skinLabel3.TabIndex = 2;
            this.skinLabel3.Text = "性　　别";
            // 
            // skinLabel4
            // 
            this.skinLabel4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.skinLabel4.AutoSize = true;
            this.skinLabel4.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel4.BorderColor = System.Drawing.Color.White;
            this.skinLabel4.Font = new System.Drawing.Font("微软雅黑", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel4.Location = new System.Drawing.Point(3, 154);
            this.skinLabel4.Name = "skinLabel4";
            this.skinLabel4.Size = new System.Drawing.Size(146, 41);
            this.skinLabel4.TabIndex = 3;
            this.skinLabel4.Text = "出生日期";
            // 
            // skinLabel5
            // 
            this.skinLabel5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.skinLabel5.AutoSize = true;
            this.skinLabel5.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel5.BorderColor = System.Drawing.Color.White;
            this.skinLabel5.Font = new System.Drawing.Font("微软雅黑", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel5.Location = new System.Drawing.Point(3, 204);
            this.skinLabel5.Name = "skinLabel5";
            this.skinLabel5.Size = new System.Drawing.Size(146, 41);
            this.skinLabel5.TabIndex = 4;
            this.skinLabel5.Text = "身份证号";
            // 
            // skinLabel6
            // 
            this.skinLabel6.AutoSize = true;
            this.skinLabel6.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel6.BorderColor = System.Drawing.Color.White;
            this.skinLabel6.Font = new System.Drawing.Font("微软雅黑", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel6.Location = new System.Drawing.Point(3, 250);
            this.skinLabel6.Name = "skinLabel6";
            this.skinLabel6.Size = new System.Drawing.Size(146, 41);
            this.skinLabel6.TabIndex = 5;
            this.skinLabel6.Text = "住　　址";
            // 
            // skinLabel8
            // 
            this.skinLabel8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.skinLabel8.AutoSize = true;
            this.skinLabel8.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel8.BorderColor = System.Drawing.Color.White;
            this.skinLabel8.Font = new System.Drawing.Font("微软雅黑", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel8.Location = new System.Drawing.Point(3, 362);
            this.skinLabel8.Name = "skinLabel8";
            this.skinLabel8.Size = new System.Drawing.Size(146, 41);
            this.skinLabel8.TabIndex = 7;
            this.skinLabel8.Text = "签发机关";
            // 
            // skinLabel7
            // 
            this.skinLabel7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.skinLabel7.AutoSize = true;
            this.skinLabel7.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel7.BorderColor = System.Drawing.Color.White;
            this.skinLabel7.Font = new System.Drawing.Font("微软雅黑", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel7.Location = new System.Drawing.Point(3, 412);
            this.skinLabel7.Name = "skinLabel7";
            this.skinLabel7.Size = new System.Drawing.Size(146, 41);
            this.skinLabel7.TabIndex = 8;
            this.skinLabel7.Text = "有效期限";
            // 
            // PeopleName
            // 
            this.PeopleName.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PeopleName.BackColor = System.Drawing.Color.Transparent;
            this.PeopleName.DataBindings.Add(new System.Windows.Forms.Binding("Tag", this.cardInfoBindingSource, "PeopleName", true));
            this.PeopleName.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.cardInfoBindingSource, "PeopleName", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.PeopleName.DownBack = null;
            this.PeopleName.Icon = null;
            this.PeopleName.IconIsButton = false;
            this.PeopleName.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.PeopleName.IsPasswordChat = '\0';
            this.PeopleName.IsSystemPasswordChar = false;
            this.PeopleName.Lines = new string[0];
            this.PeopleName.Location = new System.Drawing.Point(165, 55);
            this.PeopleName.Margin = new System.Windows.Forms.Padding(5);
            this.PeopleName.MaxLength = 32767;
            this.PeopleName.MinimumSize = new System.Drawing.Size(28, 28);
            this.PeopleName.MouseBack = null;
            this.PeopleName.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.PeopleName.Multiline = true;
            this.PeopleName.Name = "PeopleName";
            this.PeopleName.NormlBack = null;
            this.PeopleName.Padding = new System.Windows.Forms.Padding(5);
            this.PeopleName.ReadOnly = false;
            this.PeopleName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.PeopleName.Size = new System.Drawing.Size(350, 40);
            // 
            // 
            // 
            this.PeopleName.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.PeopleName.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PeopleName.SkinTxt.Font = new System.Drawing.Font("黑体", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.PeopleName.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.PeopleName.SkinTxt.Multiline = true;
            this.PeopleName.SkinTxt.Name = "BaseText";
            this.PeopleName.SkinTxt.Size = new System.Drawing.Size(340, 30);
            this.PeopleName.SkinTxt.TabIndex = 0;
            this.PeopleName.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.PeopleName.SkinTxt.WaterText = "";
            this.PeopleName.TabIndex = 10;
            this.PeopleName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.PeopleName.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.PeopleName.WaterText = "";
            this.PeopleName.WordWrap = true;
            // 
            // cardInfoBindingSource
            // 
            this.cardInfoBindingSource.DataSource = typeof(IdCardFaceIdentifier.CardInfo);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 3;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel3.Controls.Add(this.PeopleNation, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.skinLabel9, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.PeopleSex, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(160, 100);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(360, 50);
            this.tableLayoutPanel3.TabIndex = 11;
            // 
            // PeopleNation
            // 
            this.PeopleNation.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PeopleNation.BackColor = System.Drawing.Color.Transparent;
            this.PeopleNation.DownBack = null;
            this.PeopleNation.Icon = null;
            this.PeopleNation.IconIsButton = false;
            this.PeopleNation.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.PeopleNation.IsPasswordChat = '\0';
            this.PeopleNation.IsSystemPasswordChar = false;
            this.PeopleNation.Lines = new string[0];
            this.PeopleNation.Location = new System.Drawing.Point(239, 5);
            this.PeopleNation.Margin = new System.Windows.Forms.Padding(5);
            this.PeopleNation.MaxLength = 32767;
            this.PeopleNation.MinimumSize = new System.Drawing.Size(28, 28);
            this.PeopleNation.MouseBack = null;
            this.PeopleNation.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.PeopleNation.Multiline = true;
            this.PeopleNation.Name = "PeopleNation";
            this.PeopleNation.NormlBack = null;
            this.PeopleNation.Padding = new System.Windows.Forms.Padding(5);
            this.PeopleNation.ReadOnly = false;
            this.PeopleNation.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.PeopleNation.Size = new System.Drawing.Size(116, 40);
            // 
            // 
            // 
            this.PeopleNation.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.PeopleNation.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PeopleNation.SkinTxt.Font = new System.Drawing.Font("黑体", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.PeopleNation.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.PeopleNation.SkinTxt.Multiline = true;
            this.PeopleNation.SkinTxt.Name = "BaseText";
            this.PeopleNation.SkinTxt.Size = new System.Drawing.Size(106, 30);
            this.PeopleNation.SkinTxt.TabIndex = 0;
            this.PeopleNation.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.PeopleNation.SkinTxt.WaterText = "";
            this.PeopleNation.TabIndex = 3;
            this.PeopleNation.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.PeopleNation.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.PeopleNation.WaterText = "";
            this.PeopleNation.WordWrap = true;
            // 
            // skinLabel9
            // 
            this.skinLabel9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.skinLabel9.AutoSize = true;
            this.skinLabel9.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel9.BorderColor = System.Drawing.Color.White;
            this.skinLabel9.Font = new System.Drawing.Font("微软雅黑", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel9.Location = new System.Drawing.Point(134, 4);
            this.skinLabel9.Name = "skinLabel9";
            this.skinLabel9.Size = new System.Drawing.Size(91, 41);
            this.skinLabel9.TabIndex = 2;
            this.skinLabel9.Text = "民 族";
            // 
            // PeopleSex
            // 
            this.PeopleSex.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PeopleSex.BackColor = System.Drawing.Color.Transparent;
            this.PeopleSex.DownBack = null;
            this.PeopleSex.Icon = null;
            this.PeopleSex.IconIsButton = false;
            this.PeopleSex.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.PeopleSex.IsPasswordChat = '\0';
            this.PeopleSex.IsSystemPasswordChar = false;
            this.PeopleSex.Lines = new string[0];
            this.PeopleSex.Location = new System.Drawing.Point(5, 5);
            this.PeopleSex.Margin = new System.Windows.Forms.Padding(5);
            this.PeopleSex.MaxLength = 32767;
            this.PeopleSex.MinimumSize = new System.Drawing.Size(28, 28);
            this.PeopleSex.MouseBack = null;
            this.PeopleSex.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.PeopleSex.Multiline = true;
            this.PeopleSex.Name = "PeopleSex";
            this.PeopleSex.NormlBack = null;
            this.PeopleSex.Padding = new System.Windows.Forms.Padding(5);
            this.PeopleSex.ReadOnly = false;
            this.PeopleSex.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.PeopleSex.Size = new System.Drawing.Size(116, 40);
            // 
            // 
            // 
            this.PeopleSex.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.PeopleSex.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PeopleSex.SkinTxt.Font = new System.Drawing.Font("黑体", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.PeopleSex.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.PeopleSex.SkinTxt.Multiline = true;
            this.PeopleSex.SkinTxt.Name = "BaseText";
            this.PeopleSex.SkinTxt.Size = new System.Drawing.Size(106, 30);
            this.PeopleSex.SkinTxt.TabIndex = 0;
            this.PeopleSex.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.PeopleSex.SkinTxt.WaterText = "";
            this.PeopleSex.TabIndex = 0;
            this.PeopleSex.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.PeopleSex.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.PeopleSex.WaterText = "";
            this.PeopleSex.WordWrap = true;
            // 
            // PeopleBirthday
            // 
            this.PeopleBirthday.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PeopleBirthday.BackColor = System.Drawing.Color.Transparent;
            this.PeopleBirthday.DownBack = null;
            this.PeopleBirthday.Icon = null;
            this.PeopleBirthday.IconIsButton = false;
            this.PeopleBirthday.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.PeopleBirthday.IsPasswordChat = '\0';
            this.PeopleBirthday.IsSystemPasswordChar = false;
            this.PeopleBirthday.Lines = new string[0];
            this.PeopleBirthday.Location = new System.Drawing.Point(165, 155);
            this.PeopleBirthday.Margin = new System.Windows.Forms.Padding(5);
            this.PeopleBirthday.MaxLength = 32767;
            this.PeopleBirthday.MinimumSize = new System.Drawing.Size(28, 28);
            this.PeopleBirthday.MouseBack = null;
            this.PeopleBirthday.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.PeopleBirthday.Multiline = true;
            this.PeopleBirthday.Name = "PeopleBirthday";
            this.PeopleBirthday.NormlBack = null;
            this.PeopleBirthday.Padding = new System.Windows.Forms.Padding(5);
            this.PeopleBirthday.ReadOnly = false;
            this.PeopleBirthday.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.PeopleBirthday.Size = new System.Drawing.Size(350, 40);
            // 
            // 
            // 
            this.PeopleBirthday.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.PeopleBirthday.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PeopleBirthday.SkinTxt.Font = new System.Drawing.Font("黑体", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.PeopleBirthday.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.PeopleBirthday.SkinTxt.Multiline = true;
            this.PeopleBirthday.SkinTxt.Name = "BaseText";
            this.PeopleBirthday.SkinTxt.Size = new System.Drawing.Size(340, 30);
            this.PeopleBirthday.SkinTxt.TabIndex = 0;
            this.PeopleBirthday.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.PeopleBirthday.SkinTxt.WaterText = "";
            this.PeopleBirthday.TabIndex = 12;
            this.PeopleBirthday.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.PeopleBirthday.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.PeopleBirthday.WaterText = "";
            this.PeopleBirthday.WordWrap = true;
            // 
            // PeopleIDCode
            // 
            this.PeopleIDCode.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PeopleIDCode.BackColor = System.Drawing.Color.Transparent;
            this.PeopleIDCode.DownBack = null;
            this.PeopleIDCode.Icon = null;
            this.PeopleIDCode.IconIsButton = false;
            this.PeopleIDCode.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.PeopleIDCode.IsPasswordChat = '\0';
            this.PeopleIDCode.IsSystemPasswordChar = false;
            this.PeopleIDCode.Lines = new string[0];
            this.PeopleIDCode.Location = new System.Drawing.Point(165, 205);
            this.PeopleIDCode.Margin = new System.Windows.Forms.Padding(5);
            this.PeopleIDCode.MaxLength = 32767;
            this.PeopleIDCode.MinimumSize = new System.Drawing.Size(28, 28);
            this.PeopleIDCode.MouseBack = null;
            this.PeopleIDCode.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.PeopleIDCode.Multiline = true;
            this.PeopleIDCode.Name = "PeopleIDCode";
            this.PeopleIDCode.NormlBack = null;
            this.PeopleIDCode.Padding = new System.Windows.Forms.Padding(5);
            this.PeopleIDCode.ReadOnly = false;
            this.PeopleIDCode.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.PeopleIDCode.Size = new System.Drawing.Size(350, 40);
            // 
            // 
            // 
            this.PeopleIDCode.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.PeopleIDCode.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PeopleIDCode.SkinTxt.Font = new System.Drawing.Font("黑体", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.PeopleIDCode.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.PeopleIDCode.SkinTxt.Multiline = true;
            this.PeopleIDCode.SkinTxt.Name = "BaseText";
            this.PeopleIDCode.SkinTxt.Size = new System.Drawing.Size(340, 30);
            this.PeopleIDCode.SkinTxt.TabIndex = 0;
            this.PeopleIDCode.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.PeopleIDCode.SkinTxt.WaterText = "";
            this.PeopleIDCode.TabIndex = 13;
            this.PeopleIDCode.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.PeopleIDCode.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.PeopleIDCode.WaterText = "";
            this.PeopleIDCode.WordWrap = true;
            // 
            // Department
            // 
            this.Department.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Department.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.SetColumnSpan(this.Department, 2);
            this.Department.DownBack = null;
            this.Department.Icon = null;
            this.Department.IconIsButton = false;
            this.Department.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.Department.IsPasswordChat = '\0';
            this.Department.IsSystemPasswordChar = false;
            this.Department.Lines = new string[0];
            this.Department.Location = new System.Drawing.Point(165, 363);
            this.Department.Margin = new System.Windows.Forms.Padding(5);
            this.Department.MaxLength = 32767;
            this.Department.MinimumSize = new System.Drawing.Size(28, 28);
            this.Department.MouseBack = null;
            this.Department.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.Department.Multiline = true;
            this.Department.Name = "Department";
            this.Department.NormlBack = null;
            this.Department.Padding = new System.Windows.Forms.Padding(5);
            this.Department.ReadOnly = false;
            this.Department.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Department.Size = new System.Drawing.Size(510, 40);
            // 
            // 
            // 
            this.Department.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Department.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Department.SkinTxt.Font = new System.Drawing.Font("黑体", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Department.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.Department.SkinTxt.Multiline = true;
            this.Department.SkinTxt.Name = "BaseText";
            this.Department.SkinTxt.Size = new System.Drawing.Size(500, 30);
            this.Department.SkinTxt.TabIndex = 0;
            this.Department.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.Department.SkinTxt.WaterText = "";
            this.Department.TabIndex = 15;
            this.Department.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Department.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.Department.WaterText = "";
            this.Department.WordWrap = true;
            // 
            // skinPictureBox1
            // 
            this.skinPictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.skinPictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.skinPictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.skinPictureBox1.Location = new System.Drawing.Point(523, 53);
            this.skinPictureBox1.Name = "skinPictureBox1";
            this.tableLayoutPanel2.SetRowSpan(this.skinPictureBox1, 4);
            this.skinPictureBox1.Size = new System.Drawing.Size(154, 194);
            this.skinPictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.skinPictureBox1.TabIndex = 17;
            this.skinPictureBox1.TabStop = false;
            // 
            // PeopleAddress
            // 
            this.PeopleAddress.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PeopleAddress.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.SetColumnSpan(this.PeopleAddress, 2);
            this.PeopleAddress.DownBack = null;
            this.PeopleAddress.Icon = null;
            this.PeopleAddress.IconIsButton = false;
            this.PeopleAddress.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.PeopleAddress.IsPasswordChat = '\0';
            this.PeopleAddress.IsSystemPasswordChar = false;
            this.PeopleAddress.Lines = new string[0];
            this.PeopleAddress.Location = new System.Drawing.Point(165, 255);
            this.PeopleAddress.Margin = new System.Windows.Forms.Padding(5);
            this.PeopleAddress.MaxLength = 32767;
            this.PeopleAddress.MinimumSize = new System.Drawing.Size(28, 28);
            this.PeopleAddress.MouseBack = null;
            this.PeopleAddress.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.PeopleAddress.Multiline = true;
            this.PeopleAddress.Name = "PeopleAddress";
            this.PeopleAddress.NormlBack = null;
            this.PeopleAddress.Padding = new System.Windows.Forms.Padding(5);
            this.PeopleAddress.ReadOnly = false;
            this.PeopleAddress.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.PeopleAddress.Size = new System.Drawing.Size(510, 98);
            // 
            // 
            // 
            this.PeopleAddress.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.PeopleAddress.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PeopleAddress.SkinTxt.Font = new System.Drawing.Font("黑体", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.PeopleAddress.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.PeopleAddress.SkinTxt.Multiline = true;
            this.PeopleAddress.SkinTxt.Name = "BaseText";
            this.PeopleAddress.SkinTxt.Size = new System.Drawing.Size(500, 88);
            this.PeopleAddress.SkinTxt.TabIndex = 0;
            this.PeopleAddress.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.PeopleAddress.SkinTxt.WaterText = "";
            this.PeopleAddress.TabIndex = 18;
            this.PeopleAddress.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.PeopleAddress.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.PeopleAddress.WaterText = "";
            this.PeopleAddress.WordWrap = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 700F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel5, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(4, 32);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1264, 797);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel4.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.skinLabel10, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.skinPictureBox2, 0, 1);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(710, 10);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(550, 458);
            this.tableLayoutPanel4.TabIndex = 1;
            // 
            // skinLabel10
            // 
            this.skinLabel10.BackColor = System.Drawing.Color.LightGray;
            this.skinLabel10.BorderColor = System.Drawing.Color.White;
            this.tableLayoutPanel4.SetColumnSpan(this.skinLabel10, 3);
            this.skinLabel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinLabel10.Font = new System.Drawing.Font("微软雅黑", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel10.Location = new System.Drawing.Point(0, 0);
            this.skinLabel10.Margin = new System.Windows.Forms.Padding(0);
            this.skinLabel10.Name = "skinLabel10";
            this.skinLabel10.Size = new System.Drawing.Size(550, 50);
            this.skinLabel10.TabIndex = 1;
            this.skinLabel10.Text = "实时视频";
            this.skinLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // skinPictureBox2
            // 
            this.skinPictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.skinPictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.skinPictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.skinPictureBox2.Location = new System.Drawing.Point(3, 53);
            this.skinPictureBox2.Name = "skinPictureBox2";
            this.skinPictureBox2.Size = new System.Drawing.Size(544, 402);
            this.skinPictureBox2.TabIndex = 2;
            this.skinPictureBox2.TabStop = false;
            this.skinPictureBox2.Paint += new System.Windows.Forms.PaintEventHandler(this.skinPictureBox2_Paint);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tableLayoutPanel5.AutoSize = true;
            this.tableLayoutPanel5.ColumnCount = 3;
            this.tableLayoutPanel1.SetColumnSpan(this.tableLayoutPanel5, 2);
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 256F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 400F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 400F));
            this.tableLayoutPanel5.Controls.Add(this.skinPictureBox3, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.skinPictureBox4, 2, 0);
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel6, 1, 0);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(107, 487);
            this.tableLayoutPanel5.MinimumSize = new System.Drawing.Size(0, 300);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(1056, 300);
            this.tableLayoutPanel5.TabIndex = 2;
            // 
            // skinPictureBox3
            // 
            this.skinPictureBox3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.skinPictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.skinPictureBox3.Image = global::IdCardFaceIdentifier.Properties.Resources.Blue_Firewire_256;
            this.skinPictureBox3.Location = new System.Drawing.Point(3, 22);
            this.skinPictureBox3.Name = "skinPictureBox3";
            this.skinPictureBox3.Size = new System.Drawing.Size(250, 256);
            this.skinPictureBox3.TabIndex = 0;
            this.skinPictureBox3.TabStop = false;
            // 
            // skinPictureBox4
            // 
            this.skinPictureBox4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.skinPictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.skinPictureBox4.Location = new System.Drawing.Point(706, 22);
            this.skinPictureBox4.Name = "skinPictureBox4";
            this.skinPictureBox4.Size = new System.Drawing.Size(300, 256);
            this.skinPictureBox4.TabIndex = 1;
            this.skinPictureBox4.TabStop = false;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tableLayoutPanel6.AutoSize = true;
            this.tableLayoutPanel6.ColumnCount = 1;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Controls.Add(this.skinLabel11, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.skinLabel12, 0, 1);
            this.tableLayoutPanel6.Location = new System.Drawing.Point(301, 88);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 2;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(309, 124);
            this.tableLayoutPanel6.TabIndex = 2;
            // 
            // skinLabel11
            // 
            this.skinLabel11.AutoSize = true;
            this.skinLabel11.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel11.BorderColor = System.Drawing.Color.White;
            this.skinLabel11.Font = new System.Drawing.Font("微软雅黑", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel11.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.skinLabel11.Location = new System.Drawing.Point(3, 0);
            this.skinLabel11.Name = "skinLabel11";
            this.skinLabel11.Size = new System.Drawing.Size(303, 62);
            this.skinLabel11.TabIndex = 0;
            this.skinLabel11.Text = "请放身份证...";
            // 
            // skinLabel12
            // 
            this.skinLabel12.AutoSize = true;
            this.skinLabel12.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel12.BorderColor = System.Drawing.Color.White;
            this.skinLabel12.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel12.Location = new System.Drawing.Point(3, 62);
            this.skinLabel12.Name = "skinLabel12";
            this.skinLabel12.Size = new System.Drawing.Size(0, 27);
            this.skinLabel12.TabIndex = 1;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1272, 833);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1024, 768);
            this.Name = "Form1";
            this.Text = "二代身份证人脸比对系统";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cardInfoBindingSource)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.skinPictureBox1)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.skinPictureBox2)).EndInit();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.skinPictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.skinPictureBox4)).EndInit();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private CCWin.SkinControl.SkinTextBox ValidDate;
        private CCWin.SkinControl.SkinLabel skinLabel1;
        private CCWin.SkinControl.SkinLabel skinLabel2;
        private CCWin.SkinControl.SkinLabel skinLabel3;
        private CCWin.SkinControl.SkinLabel skinLabel4;
        private CCWin.SkinControl.SkinLabel skinLabel5;
        private CCWin.SkinControl.SkinLabel skinLabel6;
        private CCWin.SkinControl.SkinLabel skinLabel8;
        private CCWin.SkinControl.SkinLabel skinLabel7;
        private CCWin.SkinControl.SkinTextBox PeopleName;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private CCWin.SkinControl.SkinTextBox PeopleNation;
        private CCWin.SkinControl.SkinLabel skinLabel9;
        private CCWin.SkinControl.SkinTextBox PeopleSex;
        private CCWin.SkinControl.SkinTextBox PeopleBirthday;
        private CCWin.SkinControl.SkinTextBox PeopleIDCode;
        private CCWin.SkinControl.SkinTextBox Department;
        private CCWin.SkinControl.SkinPictureBox skinPictureBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private CCWin.SkinControl.SkinLabel skinLabel10;
        private CCWin.SkinControl.SkinPictureBox skinPictureBox2;
        private CCWin.SkinControl.SkinTextBox PeopleAddress;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private CCWin.SkinControl.SkinPictureBox skinPictureBox3;
        private CCWin.SkinControl.SkinPictureBox skinPictureBox4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private CCWin.SkinControl.SkinLabel skinLabel11;
        private CCWin.SkinControl.SkinLabel skinLabel12;
        private System.Windows.Forms.BindingSource cardInfoBindingSource;
        private System.IO.Ports.SerialPort serialPort1;
    }
}

